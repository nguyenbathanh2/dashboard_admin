import { Navigate, Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { userRows } from "./dummyData";
import Home from "./pages/home/Home";
import NewUser from "./pages/newUser/NewUser";
import User from "./pages/user/User";
//style css
import { useContext } from "react";
import "./app.css";
import { AuthContext } from "./contex/AuthContext";
import Login from "./pages/login/Login";
import NewProduct from "./pages/newProduct/NewProduct";
import Product from "./pages/product/Product";
import ProductList from "./pages/productList/ProductList";
import UserList from "./pages/userList/UserList";
function App() {

  //neu 0 co user se tro ve login page
  const { currentUser } = useContext(AuthContext)
  const RequireAuth = ({ children }) => {
    return currentUser ? (children) : <Navigate to="login" />
  }

  return (
    <>
      <Router>

        <div className="container">
          <Routes>
            <Route path="/">
              <Route path="login" element={<Login />} />
              <Route index element={<RequireAuth><Home /></RequireAuth>} />
              <Route path="/users" element={<RequireAuth><UserList /></RequireAuth>}>
              </Route>
              <Route path="/user/:userId" element={<RequireAuth><User /></RequireAuth>}>
              </Route>
              <Route path="/newUser" element={<RequireAuth><NewUser inputs={userRows} title="New User" /></RequireAuth>}>
              </Route>
              <Route path="/products" element={<RequireAuth><ProductList /></RequireAuth>}>
              </Route>
              <Route path="/product/:productId" element={<RequireAuth><Product /></RequireAuth>}>
              </Route>
              <Route path="/newProduct" element={<RequireAuth><NewProduct /></RequireAuth>}>
              </Route>
            </Route>
          </Routes>


        </div>
      </Router>
    </>
  )
}

export default App
