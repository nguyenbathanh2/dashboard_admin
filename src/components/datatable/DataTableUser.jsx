import { DeleteOutline, Edit } from "@mui/icons-material";
import { DataGrid } from "@mui/x-data-grid";
import {
    collection,
    deleteDoc,
    doc,
    onSnapshot
} from "firebase/firestore";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { userColumns } from "../../dataTableSource";
import { db } from "../../firebase";
import "./dataTableUser.css";

const DataTableUser = () => {
    const [data, setData] = useState([]);

    useEffect(() => {

        // LISTEN (REALTIME)
        const unsub = onSnapshot(
            collection(db, "users"),
            (snapShot) => {
                let list = [];
                snapShot.docs.forEach((doc) => {
                    list.push({ id: doc.id, ...doc.data() });
                });
                setData(list);
            },
            (error) => {
                console.log(error);
            }
        );

        return () => {
            unsub();
        };
    }, []);

    const handleDelete = async (id) => {
        try {
            await deleteDoc(doc(db, "users", id));
            setData(data.filter((item) => item.id !== id));
        } catch (err) {
            console.log(err);
        }
    };

    const actionColumn = [
        {
            field: "action",
            headerName: 'Action',
            with: 140,
            renderCell: (params) => {
                return (
                    <>
                        <Link to={"/user/" + params.row.id}>
                            <button className="btn_action"><Edit className="userListEdit" /></button>
                        </Link>
                        <button className="btn_action"><DeleteOutline className="userListDelete" onClick={() =>
                            handleDelete(params.row.id)} /></button>
                    </>
                )
            }
        }

    ];
    return (
        <div className="datatable">
            <DataGrid
                className="datagrid"
                rows={data}
                columns={userColumns.concat(actionColumn)}
                initialState={{
                    pagination: {
                        paginationModel: { page: 0, pageSize: 9 },
                    },
                }}
                pageSizeOptions={[5, 10]}
                checkboxSelection

            />
        </div>
    );
};

export default DataTableUser;