import {
    Assessment,
    AttachMoney,
    ChatBubbleOutline,
    DynamicFeed,
    LineStyle,
    MailOutline,
    PermIdentity,
    Report,
    Storefront,
    Timeline, TrendingUp
} from '@material-ui/icons'
import { Link } from 'react-router-dom'
import "./sideBar.css"

export default function SideBar() {
    return (
        <div className='sideBar'>
            <div className='sidebar_wrapper'>
                <div className='sidebar_menu'>
                    <h3 className='sidebar_title'>Dashboard</h3>
                    <ul className='sidebar_list'>
                        <Link to="/" className='link'>
                            <li className='sidebar_list-item active'>
                                <LineStyle className='sidebar-icon' />
                                Home
                            </li>
                        </Link>
                        <li className='sidebar_list-item'>
                            <Timeline className='sidebar-icon' />
                            Analytics
                        </li>
                        <li className='sidebar_list-item'>
                            <TrendingUp className='sidebar-icon' />
                            Sales
                        </li>
                    </ul>
                </div>
                <div className='sidebar_menu'>
                    <h3 className='sidebar_title'>Quick Menu</h3>
                    <ul className='sidebar_list'>
                        <Link to="/users" className='link'>
                            <li className='sidebar_list-item active'>
                                <PermIdentity className='sidebar-icon' />
                                Users
                            </li>
                        </Link>
                        <Link to="/products" className='link'>
                            <li className='sidebar_list-item'>
                                <Storefront className='sidebar-icon' />
                                Products
                            </li>
                        </Link>
                        <li className='sidebar_list-item'>
                            <AttachMoney className='sidebar-icon' />
                            Transactions
                        </li>
                        <li className='sidebar_list-item'>
                            <Assessment className='sidebar-icon' />
                            Reports
                        </li>
                    </ul>
                </div>
                <div className='sidebar_menu'>
                    <h3 className='sidebar_title'>Notifications</h3>
                    <ul className='sidebar_list'>
                        <li className='sidebar_list-item active'>
                            <MailOutline className='sidebar-icon' />
                            Mail
                        </li>
                        <li className='sidebar_list-item'>
                            <DynamicFeed className='sidebar-icon' />
                            Feedback
                        </li>
                        <li className='sidebar_list-item'>
                            <ChatBubbleOutline className='sidebar-icon' />
                            Messages
                        </li>
                    </ul>
                </div>
                <div className='sidebar_menu'>
                    <h3 className='sidebar_title'>Staff</h3>
                    <ul className='sidebar_list'>
                        <li className='sidebar_list-item active'>
                            <LineStyle className='sidebar-icon' />
                            Manage
                        </li>
                        <li className='sidebar_list-item'>
                            <Timeline className='sidebar-icon' />
                            Analytics
                        </li>
                        <li className='sidebar_list-item'>
                            <Report className='sidebar-icon' />
                            Reports
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
