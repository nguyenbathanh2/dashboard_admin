
import { Language, NotificationsNone, Settings } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import "./topBar.css";

export default function Topbar() {
    return (
        <div className='topbar'>
            <div className='topbarWrapper'>
                <Link to="/" className='link'>
                    <div className='topLeft'>
                        <span className='logo'>Thanhadmin</span>
                    </div>
                </Link>
                <div className='topRight'>
                    <div className='topbarIconContainer'>
                        <NotificationsNone />
                        <span className='topIconBadge'>2</span>
                    </div>
                    <div className='topbarIconContainer'>
                        <Language />
                        <span className='topIconBadge'>2</span>
                    </div>
                    <div className='topbarIconContainer'>
                        <Settings />
                    </div>
                    <Link to="login" className='link'>
                        <img className='topAvatar' src="../../../src/assets/images/catAvatar.jpg" alt="" />
                    </Link>
                </div>
            </div>
        </div>


    )
}
