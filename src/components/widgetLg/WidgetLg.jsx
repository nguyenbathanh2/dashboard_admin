import "./widgetLg.css"

export default function WidgetLg() {
    const Btn = ({ type }) => {
        return <button className={"widgetLg_btn " + type}>{type}</button>
    }
    return (
        <div className="widgetLg">
            <h3 className="widgetLg_title">Lastest transactions</h3>
            <table className="widgetLg_table">
                <thead>
                    <tr className="widgetLg_tr">
                        <th className="widgetLg_th">Customer</th>
                        <th className="widgetLg_th">Date</th>
                        <th className="widgetLg_th">Amount</th>
                        <th className="widgetLg_th">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="widgetLg_tr">
                        <td className="widgetLg_user">
                            <img className="widgetLg_img" src="../../../src/assets/images/ninja.jfif" alt="" />
                            <span className="widgetLg_name">Ninja K</span>
                        </td>
                        <td className="widgetLg_date">2 Jun 2021</td>
                        <td className="widgetLg_amount">$122.00</td>
                        <td className="widgetLg_status">
                            <Btn type="Approved" />
                        </td>
                    </tr>
                    <tr className="widgetLg_tr">
                        <td className="widgetLg_user">
                            <img className="widgetLg_img" src="../../../src/assets/images/ninja.jfif" alt="" />
                            <span className="widgetLg_name">Ninja K</span>
                        </td>
                        <td className="widgetLg_date">2 Jun 2021</td>
                        <td className="widgetLg_amount">$122.00</td>
                        <td className="widgetLg_status">
                            <Btn type="Declined" />
                        </td>
                    </tr>
                    <tr className="widgetLg_tr">
                        <td className="widgetLg_user">
                            <img className="widgetLg_img" src="../../../src/assets/images/ninja.jfif" alt="" />
                            <span className="widgetLg_name">Ninja K</span>
                        </td>
                        <td className="widgetLg_date">2 Jun 2021</td>
                        <td className="widgetLg_amount">$122.00</td>
                        <td className="widgetLg_status"><Btn type="Pending" /></td>
                    </tr>
                    <tr className="widgetLg_tr">
                        <td className="widgetLg_user">
                            <img className="widgetLg_img" src="../../../src/assets/images/ninja.jfif" alt="" />
                            <span className="widgetLg_name">Ninja K</span>
                        </td>
                        <td className="widgetLg_date">2 Jun 2021</td>
                        <td className="widgetLg_amount">$122.00</td>
                        <td className="widgetLg_status"><Btn type="Approved" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}