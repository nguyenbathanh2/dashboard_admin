import { Visibility } from "@mui/icons-material"
import "./widgetSm.css"
export default function WidgetSm() {
    return (
        <div className="widgetSm">
            <span className="widgetSm_title">New Join Members</span>
            <ul className="widgetSm_list">
                <li className="widgetSm_list-item">
                    <img className="widgetSm_img" src="../../../src/assets/images/gacute.jpg" alt="avata-ga" />
                    <div className="widgetSm_user">
                        <span className="widgetSm_user-name">Gà Cutte</span>
                        <span className="widgetSm_user-title">chicken rice is so good</span>
                        <button className="widgetSm_btn">
                            <Visibility className="widgetSm_icon" />
                            Display
                        </button>
                    </div>
                </li>
                <li className="widgetSm_list-item">
                    <img className="widgetSm_img" src="../../../src/assets/images/gacute.jpg" alt="avata-ga" />
                    <div className="widgetSm_user">
                        <span className="widgetSm_user-name">Gà Cutte</span>
                        <span className="widgetSm_user-title">chicken rice is so good</span>
                        <button className="widgetSm_btn">
                            <Visibility className="widgetSm_icon" />
                            Display
                        </button>
                    </div>
                </li>
                <li className="widgetSm_list-item">
                    <img className="widgetSm_img" src="../../../src/assets/images/vitcutee.png" alt="avata-vit" />
                    <div className="widgetSm_user">
                        <span className="widgetSm_user-name">Vit Cutte</span>
                        <span className="widgetSm_user-title">Vit rice is so good</span>
                        <button className="widgetSm_btn">
                            <Visibility className="widgetSm_icon" />
                            Display
                        </button>
                    </div>
                </li>
                <li className="widgetSm_list-item">
                    <img className="widgetSm_img" src="../../../src/assets/images/dogcutee.jfif" alt="avata-dog" />
                    <div className="widgetSm_user">
                        <span className="widgetSm_user-name">Dog Cutte</span>
                        <span className="widgetSm_user-title">Dog is my friend </span>
                        <button className="widgetSm_btn">
                            <Visibility className="widgetSm_icon" />
                            Display
                        </button>
                    </div>
                </li>
            </ul>
        </div>
    )
}
