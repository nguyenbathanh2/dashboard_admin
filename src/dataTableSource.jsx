export const userColumns = [
    { field: "id", headerName: "ID", width: 70 },
    {
        field: 'avatar', headerName: 'Avatar', renderCell: (params) => {
            return (<div>
                <img className="img_avatar" src={params.row.img} />
            </div>)
        }
    },

    {
        field: 'userName', headerName: 'UserName', width: 200, renderCell: (params) => {
            return (
                <div>
                    {params.row.username}
                </div>)
        }
    },
    {
        field: "email",
        headerName: "Email",
        width: 230,
    },

    {
        field: "phone",
        headerName: "Phone Number",
        width: 180,
    },

    {
        field: "address",
        headerName: "Address",
        width: 100,
    },
    {
        field: "country",
        headerName: "Country",
        width: 100,
    }
];