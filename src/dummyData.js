export const userData = [
    {
        name: 'Jan',
        "Active User": 4000,
    },
    {
        name: 'Feb',
        "Active User": 3000,
    },
    {
        name: 'Mar',
        "Active User": 2000,
    },
    {
        name: 'Apr',
        "Active User": 2780,
    },
    {
        name: 'May',
        "Active User": 1890,
    },
    {
        name: 'Jun',
        "Active User": 2390,
    },
    {
        name: 'Jul',
        "Active User": 2390,
    },
    {
        name: 'Agu',
        "Active User": 2390,
    },
    {
        name: 'Sep',
        "Active User": 2390,
    },
    {
        name: 'Oct',
        "Active User": 2390,
    },
    {
        name: 'Dec',
        "Active User": 3490,
    },
];

export const productData = [
    {
        name: 'Jan',
        "Sales": 4000,
    },
    {
        name: 'Feb',
        "Sales": 3000,
    },
    {
        name: 'Mar',
        "Sales": 2000,
    },
    {
        name: 'Apr',
        "Sales": 2780,
    },
    {
        name: 'May',
        "Sales": 1890,
    },
];


export const userRows = [
    {
        id: "username",
        label: "Username",
        type: "text",
        placeholder: "john_doe",
    },
    {
        id: "displayName",
        label: "Name and surname",
        type: "text",
        placeholder: "John Doe",
    },
    {
        id: "email",
        label: "Email",
        type: "mail",
        placeholder: "name@gmail.com",
    },
    {
        id: "phone",
        label: "Phone",
        type: "text",
        placeholder: "+1 234 567 89",
    },
    {
        id: "password",
        label: "Password",
        type: "password",
    },
    {
        id: "address",
        label: "Address",
        type: "text",
        placeholder: "Elton St. 216 NewYork",
    },
    {
        id: "country",
        label: "Country",
        type: "text",
        placeholder: "USA",
    },
];
export const productRows = [
    {
        id: 1,
        img: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYSFBgWFhUYGBgaGB4aGhwcGRwcHhwcHBgaHRwaHhkcIS4lHB8rHxkYKDgmKy8xNTU1HSQ7QDs0Py40NTEBDAwMEA8QHxISHjQrJSwxNDQ0NDQ0NDQ0NDQ0NDQ0NDQ2NDQ0NDQ0NDY0PTQ0NDQ0NDQ0NDQ0NDQ0NDU0NDQ0NP/AABEIAQMAwwMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABQYDBAcCAQj/xABDEAACAQIDBQYCBwYEBQUAAAABAgADEQQSIQUxQVFhBhMicYGRMqFCUnKCkrHBFCNistHwJHOi4RU0Q8LSB2ODk/H/xAAYAQEAAwEAAAAAAAAAAAAAAAAAAQIDBP/EACYRAQEAAgIBAwMFAQAAAAAAAAABAhEhMQMSQVEigZEEFGFxoRP/2gAMAwEAAhEDEQA/AEREBERAREQEq/aTtF3ZNKkfHud/qdB/F14ee6b2xizRoO43hfD9okKvzInMCb7zc8SePWAZiTckknUk6knmTPkRA9pTZvhUnyBP5TI2Ecb0P99JKYbbGVQiIFBBGYm53G2lrcpq95VcArmvqTYbrWIPTefaW1Eo6JIU9j1qmoTjxZR+s81Nk1l302PlZvkDeRqmqluym2Cjik7Eo2i3PwNwHkd3nbrLzOSOhU2YFTyIIPznRuzm0f2iiCfjXwP1IGjeosfO8hCViIgIiICIiAiIgIiICIiAiIgIiIED2ze2GtzdB+bf9soEvvbYf4cf5i/ytK5sjY3eWd7hDqBxYDj0H5/OTJtMm0ZhsI9Q2RSdbE8Bfmdwkrh9iAC7tfW1l3bue/8AKWFECogUADNew8jNavon3v6zSYxb08NjAYCmg0RfMi59zrMFamFIIAABOg5cfleSOGGk1sYnhb70strhq7KYqXQ71NvMfRPtabtRtx9fb+/nPVfBZKS4kcXNNv8A6ldf5XmriHtn/hIb0YWMSp1plqIKgKsAQRxF9fWYezaLSqOu7vALDhdc36E+090HuQRz/T/cGa+L8DhtQLnUcOII6g6xZLFLNrXExYarnQE7+PmN9unH1mWYKEREBERAREQEREBERAREQERNPauK7mi7jeqG32jovzIgV7b2M/aMQmGU+BW8f8TAEkeQAI8yeUk6yhSANwSVHs2L4hWJ3XYnmW8Ovq4lsxvxN9kj5EzXDppjPpYi9kW/AqfnlP8ANMGKoswyqNRdvwKxPymN611H1TmRjvsG1062sbyw9ndks70nq5VVGuxfxBx0Qa+IH+90nK6i0m+mpgDmQH1+U+VMM9W6IpZjpYC+8E8Oms6pgez+DvnWmrX53I88p0v1tc8dZNU6aoLKoUcgAPymV8ifbTntLsy7YJVcMGNRnKu5RQFpuq3XdckLqRexldTYGjKz08zEIbPwFtfM33eUtvbnZ2JxDKKZuljZRfUgXN+lvc36SoYXs7UIUVFakzM+VmceIIpNhTI52F78OsmXje0/3Fd2a5WrkOouynoUvY+02tojQg8Cp9CSJ6bZ7UsRZjclLn7WYoR6FTPuLOZshGhU2PUEGx6aia4q5TXDb7PYi6FCdVNvbcfa3tJmVTZ4ZKhdeBUMPtXt81t96WlHDAEbjMs5yyr1ERKoIiICIiAiIgIiICIiAkR2pplsLUsL2ysfIMpJ9ACfSS8EQOfdmVys1QgEAoovzarTDEeQK/iEsVV/32Xhp80YfoPeSTbKpL8KBbCwtcKPGr3y3t8Sqb24CQ2Ka1e/8SH00U/zTXDppOIx7JISm7tqUstuo0Unh0kp/wAAxVZjmRrgKx3myst1IAFzqbG2gN9++eezDrSxjq9sr+HXddjcH3Uj1nUl2elTIHQMEHh1OgtYiw+ja2m72lMstVvJvGVj7F4RqVJlYZTmHgDs6p4RdLsT4gQb68ZY7Txh6SooVVCqBYACwA5ADdMjk2AA46+Uyt3dsreWDE4ZXFmFxv3kajcbjUGRtTZVNCWVBnItmJLPblmYk26XtJqeSt5G1sbpxjtAMuLqjkyKOn02HuzSKds2cDepBHnlGnz+U2Nt1mGKzHUVaruD0dAyewNvSRyPlxDqTZWGvSyhgZ149K5Xd22sA4yVTxCqfwm6/PT0EnGHdm4+E7+nXy5yEw1OzlT9MqD5Xvf1tbpLCZnnxWdImPDnw25ae275WmSUVIiICIiAiIgIiICIiAn1N4858gGCNXaVTLSc8bfMkD9ZCbWXx5xuIF/UHX1HzHlJHbdW5SmPpMGPlfT5/lIraNUDu3+iVKt9nMB7jQjqBNsJwvJxXutixRqJVZFqI6ZHU2tfeDqN4I/OdH7FdqqNd2w92BXSmalsxsPEhNzcqb2O8r5EzmzUc9N6LbxexHNeI/vWaVLMrO6Eg3FRGGmp4g8wSkrljteZXT9IzHWewvYnoBczm3ZP/wBSw6ZMUpuuhqILg9WTePu38hOg4PaNKr8FRW6X1Hmp1HtMLjYT5ZqbMd65R53PsJ5xeJWkjO7BVVSzE6AACfcViUpKXdgqjeTp/wDp6Tl//qNtV61JRdkps4ypuZwPpvy1K2The510E4zd0mS3lSa9Z6mZ+NMoyeSpZR+EGZcWmZ86jR6RZep0FvOxtNXDZgpFyD4lGmgyrnDHjpb5+U38PVWojKPDqcnS1ibe4NvOdM4VrG75kVgfXqND7gXk3hsZZF1DErdddRzDDkDxkPhMLmQA+EZiW6b7gfObNCsiC6oMgazk6k6fF1tf5RlNxXXymsMtl89f6fITLMKDIbX8J3dP9pmmN7VpERIQREQEREBERAREQE8VHyjruA5k7p7kXt3Fd2i23sSB08J19B+kmTdGpTbva+S9xewbje3iPkRp+Hlro4ykVpKjfRZ6Z8x4l+aiZNgOqO7ObEJYDeTc3NlGp3cJsbWcOvh17x1cc9UUbvMzad6XnTUZ2pujcCiG/UIAR6hfmZkrLlrhV+F6ZKj7pIt+EaTb2rSU0Xt9BkA6EZQf5jPOy0FREc/FRZvwMjafiAk1bXOkbsiwUta1zzHDyA4CdI7L0TisFTqZQ5UZHUi5GXcet1yn1nPtk7IxGJpqKNO6tcF2OVBfQ6nU6aaA751XsJsxsHnoswe6o+gsFbVGUcxlVNeNjoJj5LNaaYZZYTcjGuCW4OW5G65Jy+QPw+krva3BGpcW+GhUdeeZHpnQcdJ1CthVfUix5j+9ZXtvYFkC1QuYJcMAN6MLNpzFla3HLM8by2/6Y5zWtOP12vkyCwdnV+p7oAA/n6TRwaszUQu+xPuzBj+ES07c2bToPQalYUmfLYEnxWQhhe+ljltw/LSwWz+7SqbhmFMZSBawfMxB6hlYHynTLHPnjfVr4aWKxeht8Nyo65Rdj63WfNjVcwZDx19981K6XVAN9i463Yi3somzsRTq2VrB1BYWIX4gQdbjRwb2tpvlumc3bwl9lYnMhpt8VMlfMA/oLSXlbR8rVnG9HDD8ZRh5HT2ljVrgHnrMc5qq19iIlEEREBERAREQERPNS+U5bZrG1917aX6XgeK1dUte5J+FQLs3kP13DjaVztDnNWnnAVcpsAb2LtxO69k4ddTJLZ+1KKEq5KVjYP3gsSeFm+HLvsAbazJtTD99u8IHw3GrEXIJ+qo8R56ma4Yzt3ft/HPF6vVu3qRA4Z2FRkXTvAo87A6X9/aSWJRaVnbUqL9AEFlQepGvEyLxKkAN8LI2U9DvX5gzPtTEd4FtxtceQzEe5HtNXNvjlp0saWp1EO9mVvckn5rJ/sxhw7ujfAygtb6umb5EyuVaGUg/+2L+Ydv6yZTaDYUM1O2YKW1F9FW2o5Zig9ZS3WKcLqy103sPhb0DTbU0XekeF8rXB/CyywYHCBGdhxP5bpQuwfa6gqua7rR75g65j4QyrkZcx0vZUIvqb8bGX9NqU2AKFmB3FUYj8VrTmynK9yt4nTfnxluLGatLHo3GxHAgg+xkF2g7V0qQeijg18vwrrkBIGZjuU+IWB11BtaRJbdKarn3aREFXEZVbKhdgqniqnKwHRtbcgPKY8CytnA3VVSoOoIsf5vzlZwm3GfFi5tTZ2XLzDnKCeZ0X287zz0u5p0bH4Lr6A7va4nTjym2b3EHiUKhOYBT7yOQR66TNh6/dsXG50KsPQlTbmGA9zNnalIEkHQVDdTye1iPW1/vCR+HQtZDocxvfQC28k8BoTL1SWy7jPTzFgi6l1ObyuGv01F7+csa4vIP3iNTtpmOqeeddFH2ss0+zxFQuyr4BYZiNXbfu+io5dQT0nSJXLH1O7wfoZ5fH6rdX2YomqcI1PWjqvGmTYfcb6H2fh+zvmXC4laiB13XI6ggkEHqCDMssbj24/N4MvDlrKMsREqxIiICIiAiJobcx3cUHcfFbKn2m0Htv9IELtKoKmIzjXKy4eluP7xjd3+6D7gTb2ntBqeJp0Vtlam17i5sb2sefg+cgNlVb06bjU4euXcbzkfLd7ccpBvJPaw/x1J7+EjID9pGZDfkc+n2TNMbr/GuN1Nxv7Yw1jVNtDSRvvKxW/sR7SEom6Angxv6j/Yyz9oaoWk3NlVRz35j8re8h/8AhhR0VvqKzDrbQHyN/abbTlOeGCjQLMLjVjoOg1tMfaVyFWmhFnBNRuikELf6tzfqQJI00LsxUX3qPJfi+dx6SM2mwrCpTAs1JVqL/ELeMW5WKkeUpleEXpBYfEi5Vr5GAU6XK5fhcDmLm44hmHGeVq1cMxCVGRhxRmUEWuGBUi4IIIPIzELGb1KkKqZNc6glDzG80+vEjrccZirJvj8PLbdxRFjisQR1rP8A+Ukdj3pYWvXvqzZR5gEKfxPf7kgWQeks+CwxqYI0xfOc1RFt8So4v667uVona2E5t+Ir2zEvWT7YJPRTmJ9gZfccbof8xvyH9ZV9h7PL1EFrDR6hO5UHiRftNbNblbrLcyZkDEb7sB1c3A9FtNfHDGcVoPQ7xMh4i69GA/X+kis5KFGIDnwAnjYgkE8L2AuecncN4rkfQcqPugfrIzbWFyYhcugZgy8gTlP9Jfss42sGw6Hd0EBFjYlvMk3vNLanaejRuFPeMOCnQHq270FzIDbe12CrRKg0vFpdlY+I+HMD9EZdCCNd0gD3XAVPIlf5rfpM8s7OI7cv12WOEwwnU7XXB08Ri1zVqoSk25aZW7DkWF7D19pN4bDrSUKihVG4D+9T1lK7LbXNOr3b6I5AHJW3LbodB7HnL1M7duDLPLO7yuyIiQoREQEREBKR22x2eotIHRBmb7TDT2X+aXLFYhaaM7blUsfQbpy6riS9RqjalmLEcNTu8uEDHRqsjBkYqw3EGxHqJaNjO2LXxgg0wAzhQFK5rqulstVWJKkA7yCLE3jsI+BAzOtZj9QEAeWYW09Zbtm189A1Mgp0wP3dNRoq7i5sPE7am/LdvN7Yzlpjj/L3gAtaqzv8NPVVPE63byFvUg/VnjaVTK7sd4UE/hvb0vb0kHsPFuq1q7EhXPd00J8LM3IfwgAXG/Wbr4k1crNYM6K5HQmzfpNZlvleZcN3ZVqKPmOqg5j95ix95pbPwwZ2zNdqAYMx17yg6lla/EjT585L7UwLBnZQWRwcw4i48X9ZXOzLsuLWixucj0weaZc638iDp1twlcuonriqip0E994RqCQeYNteYkztHYFRKj5VQJe65qtJbAi9rM4Ol7ekvPYjsyuEwdfaGJp0nbuyMOjtTZCTYK5ckr4nKqDfQX5zJh057Ww5qvTYWHe3zclZT+8NuAtZvWb2xMYzYoFDlQeLXciU1IW/Lw+E/aPGxGyuAK4R/EhYuAD3lFVUsq94MwqZACFsNbkfRAOnxtlPRoJSUr3lfxVfGpIQHwKoBuynUlluNN9pOmtmtX55TGFxIxDNkUJTd7CwsWF/G587WHRT0Amdpmw04Xt/fKaew8IFdABYIpt7Zf1nrF1S9fLmTL3TkLmu7HUZsoGgGVt5G86bprLpadcvGwaX7tgdWL5vPMo/UNMW3MN3ys9JgWW4Uj6yaEetmHpPr4dzRdabZXKkA7uo14HVh6yA7NbW7hQr3CZij80Ym6v0F86nyEXLVRuakpZcbRYro4sSPqvbRvskAj24rKxUQqSrCxG8S809lGhjVZB+6xCuum4HKWt5XUMPXlIvtKwpst0V0YEa6FWW17MN11ZD6mVy5m72pljxuqwJ0rs/tD9ooKxN3Hhf7Q4+osfWc9qVUt4UtfiWv7Sa7GYzJXKE+F1/1KCQfbN8pmzXuIiAiIgIiIFa7bYopSRB/wBRtfspY29yvtKPLL25q3rIn1Uv6sx/8BK1A+otyBzIHuZ1Ps6VrUEVd3iUjd8KlLf30nMcLTDNdiQqgs1t9hwHUkqB5y8YfbK4FcO7JfvQzVMvAEKQFB4AMNPM7yb2jXxWS7qr7dxDtVAy5ETw0lA0AB/muBfkRbhLNsjBjFph6qsFNNXpVF3fRYr/AKivoek1e0uLwNW9WnXqK7fGqKyhjzYMAAev5zf7EbMyB2bOudVCqTY2uSHZRoCdwB4BjxiVOM+rXaxU64YKraE2VhxB3fnrNGvs5Kdbv2Xx0lcggfHdGtcDoSdOsjRttWxBpKpJWplLg+G4awGoBJJ09+Alm27UKUXqLbMlNmF+ajMt+l7xa14yn9KZs3suuLx4RsRReki567U2YhKVJVWzOVC5mAAurMB4jfSdN2rgztahgqCqaeGqE13y6Ww6aUU6M4embcMrfV1qVRMFh8EtClUrI+02U3REcqmYBadi6ZUu1rXJsWE6DUx1KlicJgKVZVdFBemFYsaSUnCLdVKp4grWYjQC3xa1ct7U/afYGli6+IpUSMPRw+mVRfNVahTZdCdwBNzv3dZE1sAO/NS3/TSnT6AAs1vdfnLRS7RKNsUqFJ70HNYNbUNW8RJLcQrAqOXlaR+0MP3ZKgkrT0HhGoD5QSb+Hwrw3kiXx7bY83X8RrbOoHOVA8TLlA6swUfMiRRw1DC4uqwZFOS2W7O/gJFeo7fCpNRWGmgtYDS0ltqN3aO93UBWJZLZgD4rrfiJy/FY4ZClPNZzd3a2Zze9rD4Uvra51i33Mr6bHTsGFIV0N1ZQynmp/UbpTu1+FFCsKqqClS61F3Anj5EgXB4Fbyydk6n+BpE/RDj0V3kPtrGU6uHZ3UsuclQDY6OVGvl+cW7ictXFIdj8RnoAZs6o5VCfiC5QQrcmFyNNLWlU29XL08xFs2IqZQd+VadNN3DUCWfsK5qU6zBAlNSoVRc6gEsSx1ZjddeglQ7T1y2IdTYKjFVCiwFzmY+ZZiSZFu4rlfpiImTD12purr8SsGHpw/SY4lWLrVKoHVWG5gGHkRcT3Ifspie8wya6pdD90+H/AElZMQEREBEReBzjtRWz4qpyUhR91Rf53kTMuJq947v9d2b8TE/rMUDLh3AzBr2ZcptvHiVgbcdVGnK8l8Viv2mgig/vKeXw8SAuU5eZ0U26SDiTtaXXCUweM7k3ZFR/rGlmYdVDHKp362ko/a0qvd0kJFvE9RiXYneSVIsfX2lXn0G2okSkys6WXspQfFYmmbBKVJxUbKLKCuoGt8zMbDUk2Jlv7VY3vKLU0dFz+EuzqqKpazEkm53EWUE67pzYbTrWyiowHIWA9QBr6zUdixuxJPM6n3hMz1NLWlXDNUp1GxNQJhVppTFOme8qMl2zqX8FJcx3tc6fCYxHaaqMy4f/AAyOSXKuz1ahbe1XEN43Op3ZRrulZw9bKdRcHfz8xNzLxGol8ZCarfTENRXDVKbZXTOyMADlIqNY2It7yyY7aT18PTr0XJq01y1kvckEDMSv0lJF7/rKvXX91TPIuv8AqDf9010JBBBII3EaEeRlrGuV1ftHTdiY+ni6VgQWAsyEi9jwtK8exVMMVcnLmJR1YKQpPwsrg7uDLfra2tNq7RcNdTZluAw0br4hv9eU91tv4l1ytXcj0B/EBf5zLLvhW543uLhW2qKbCnTTJh6KsGZgVDMUZVVb/ESzDXjqepilpYesi0zihSVbXUozMbcrcePnKxiMU9Q3d2Yjdc3tMMnalz26Hj9t0aFBcLgrkEeOqRoov4nJP0t5/uxou0cR3lV3G5mJHluHyAmuXNrXNuV9PafJCLlaREQquHYSvpVS+t1ceoyn8l95bZzHYuO7iur/AEb5X+y2h9tD6TpwMBERASC7W7R7qjkU+OpdR0X6R9jb1k7Oabex/wC0V3e91Byp9lePqbn1gRsREBERAREQEREBNjDYjJodV/LqJrxJl0mXSy0VD0WtrlcMPJhlPzCSPxt1UkaG9rzY7MVhmamx0dSPfT5HKfSedsUyqEEWIYA9DqJrveO2+XOEy+yEiImLnIiICIiAiIgJ0js1i+9w6G92UZG810F/Ncp9ZzeTnZPaBpVwhPgqWUjr9E+dzb1gdBiIgRnaLFGlh3Zd9goPLMQt/S85rOo7WwnfUXTiy+H7Q1X5gTl0BERAREQEREBERAREQNrZtTLVQncTY+un52k72mOamrgfGwV+jqDv+0LH3lYElaG0wzuKl+6qeFgNSoHwuB9ZTr1uZeZaxsa4ZT03G+6KibGNwjUmKtbmCPhZT8LqeKkTXlGdll1SIiEEREBERASa7K4XPiEJGiXc+m7/AFFfaQyKSbAXJnRezmyv2en4vjexboOCDyufUmBLxEQE592p2UaFUuo8DsSP4WOrL+ZHTynQZrY/BrXpsj7mG/keDDqDA5XEz4vCtSdkYWZTY/oR0IsZggIiICIiAiIgIiICIiBv4bFqyilVvkHwsBdqZO8gfSU8V9Rrv1sTh2ptla264IN1YHcyniJhm1hsVlGR1z0yb5b2Kk/SVvot8jxELb3xfy1Ym5isHlGdDnp7s1rFSfouv0W+R4GacIss7IiIQREQLH2Kw6vWZm1KJdfMm1/MD85epROxI/xDf5bfzJL3AREQERECg9tP+Z/+NfzaQE6Htvs+mKYNnKMFy3tcEXJFxpzPGVzG9lK1NWZWRwoJNrhrDkpGunWBX4iICIiAiIgIiICIiAiIgZcPXam2ZDY2seIIO9WB0YHkZsPRWqpamMpUZnTfYcXUnUqOKnUdRu0pmwmINN1dd6m9uBHFT0IuD5wtL7XphibO0aSrUcL8N7r9lgGX/SwmtCLNXRJLY2ymxTZV0VdXY8Ad2nEmx06THs3ZdTEE5Fva2YkgAX3b/I7pe9gbJ/ZUKlszM2ZiBYbgABz8+sIbOzdnph1yILcyd7HmTNuIgIiICIiAgiIgcx2zs84eqyEHLe6Hmp3e24+U0J1TH4FK6FHFwdx4qeDA8DOXVUysy/VYj2JH6QPEREBERAREQEREBERAREQPTuW38gPRQAPkBPMRAtvYN9ay9FP84P6S4SndhsM2d6n0MuTza6t8h+cuMBERAREQEREBERASj9tMKlN1dVCswJJHE87boiBWoiICIiAiIgIiICIiAiIgIiIF57D/APLv/mn+RJZIiAiIgIiIH//Z",
        name: 'Applepods',
        stock: "123",
        status: "active",
        price: "$120.00"
    },
    { id: 2, img: "../src/assets/images/avatarUser1.jpg", name: 'Cat', stock: "cat@gmail.com", status: "active", price: "$40.00" },
    { id: 3, img: "../src/assets/images/avatarUser3.jpg", name: 'Anime', stock: "anime@gmail.com", status: "active", price: "$90.00" },
    { id: 4, img: "../src/assets/images/avatarUser4.jpg", name: 'Vip', stock: "handsome@gmail.com", status: "active", price: "$520.00" },
];