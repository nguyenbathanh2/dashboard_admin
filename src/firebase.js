import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD3q7G236PTSClnA_NzUUWK49GHd-Pz-9k",
  authDomain: "coit-b74be.firebaseapp.com",
  projectId: "coit-b74be",
  storageBucket: "coit-b74be.appspot.com",
  messagingSenderId: "437986643209",
  appId: "1:437986643209:web:30989df0227c5958a8d7d8"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth();
export const storage = getStorage(app);