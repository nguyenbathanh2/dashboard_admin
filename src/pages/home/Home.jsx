import Chart from "../../components/chart/Chart"
import FeatureInfor from "../../components/featureInfo/FeatureInfor"
import SideBar from "../../components/sideBar/SideBar"
import Topbar from "../../components/topBar/topBar"
import WidgetLg from "../../components/widgetLg/WidgetLg"
import WidgetSm from "../../components/widgetSm/WidgetSm"
import { userData } from "../../dummyData.js"
import "./home.css"

export default function Home() {
    return (
        <div><Topbar />
            <div className="sidebar_home">
                <SideBar />
                <div className="home">
                    <FeatureInfor />
                    <Chart data={userData} title="User Analytics" grid dataKey="Active User" />
                    <div className="homeWidgets">
                        <WidgetSm />
                        <WidgetLg />
                    </div>
                </div>
            </div>
        </div>
    )
}
