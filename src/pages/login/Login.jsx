import { signInWithEmailAndPassword } from "firebase/auth";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../contex/AuthContext";
import { auth } from "../../firebase";
import "./login.css";

const Login = () => {
    const [error, setError] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { dispatch } = useContext(AuthContext)
    const navitage = useNavigate()
    const handleLogin = (e) => {
        e.preventDefault()
        // firebase check authentication
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in 
                const user = userCredential.user;
                dispatch({ type: "LOGIN", payload: user })
                navitage("/")
                // ...
            })
            .catch(() => {
                setError("wrong email or password !")
            });
    }
    return (
        <>
            <div className="header_backgroud">
                <header className="header__container">
                    <nav className="header__login">
                        <h1 className="header__title">Dash Board</h1>
                        <marquee className="header__quee" onMouseOver="this.stop()" onMouseOut="this.start()" direction="">WELCOME TO WEB BA THANH NGUYEN ^.^ !
                        </marquee>
                    </nav>
                    <div className="login">
                        <form className="login_form" onSubmit={handleLogin}>
                            <input className="login_input" type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
                            <input className="login_input" type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
                            <button className="login_btn" type="submit">Login</button>
                            <span className="login_span">{error}</span>
                        </form>
                        <div>emal: thanh@admin.vn</div>
                        <div>pass: 123456</div>
                    </div>
                </header>
            </div>
        </>
    )
}
export default Login