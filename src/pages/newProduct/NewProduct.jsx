import "./newProduct.css"

export default function NewProduct() {
    return (
        <div className="newProduct">
            <h1 className="newProductTitle">New Product</h1>
            <form className="newProductForm">
                <div className="newProductItem">
                    <label>Image Product</label>
                    <input type="file" id="file" />
                </div>
                <div className="newProductItem">
                    <label>Name Product</label>
                    <input type="text" placeholder="name product" />
                </div>
                <div className="newProductItem">
                    <label>Stock</label>
                    <input type="text" placeholder="123" />
                </div>
                <div className="newProductItem">
                    <label>Active</label>
                    <select className="newProductSelect" name="active" id="active">
                        <option value="yes" key="">Yes</option>
                        <option value="no" key="">No</option>
                    </select>
                </div>
            </form>
            <button className="newProductBtn">Create</button>
        </div>
    )
}
