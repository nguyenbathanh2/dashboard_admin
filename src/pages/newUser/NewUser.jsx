import { DriveFolderUploadOutlined } from "@mui/icons-material"
import { createUserWithEmailAndPassword } from "firebase/auth"
import { doc, serverTimestamp, setDoc } from "firebase/firestore"
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage"
import { useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import { auth, db, storage } from "../../firebase"
import "./newUser.css"

const NewUser = ({ inputs, title }) => {
    const [file, setFile] = useState("");
    const [data, setData] = useState({})
    const [per, setPerc] = useState(null)
    const navigate = useNavigate();

    useEffect(() => {
        const uploadFile = () => {
            const name = new Date().getTime() + file.name;
            console.log(name)
            const storageRef = ref(storage, file.name);
            const uploadTask = uploadBytesResumable(storageRef, file);

            // Register three observers:
            // 1. 'state_changed' observer, called any time the state changes
            // 2. Error observer, called on failure
            // 3. Completion observer, called on successful completion
            uploadTask.on('state_changed',
                (snapshot) => {
                    // Observe state change events such as progress, pause, and resume
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log('Upload is ' + progress + '% done');
                    setPerc(progress)
                    switch (snapshot.state) {
                        case 'paused':
                            console.log('Upload is paused');
                            break;
                        case 'running':
                            console.log('Upload is running');
                            break;
                        default:
                            break;
                    }
                },
                (error) => {
                    console.log(error)
                },
                () => {
                    // Handle successful uploads on complete
                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                    getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                        setData((prev) => ({ ...prev, img: downloadURL }))
                    });
                }
            );


        }
        file && uploadFile()
    }, [file])

    const handleInput = (e) => {
        const id = e.target.id;
        const value = e.target.value;

        setData({ ...data, [id]: value });
    };
    const handleAddUser = async (e) => {
        e.preventDefault()
        try {
            const res = await createUserWithEmailAndPassword(
                auth,
                data.email,
                data.password,
            );
            // Add a new document in collection "user"
            await setDoc(doc(db, "users", res.user.uid), {
                ...data,
                timeStamp: serverTimestamp()
            });
            navigate(-1)
        } catch (err) {
            console.log(err)
        }
    }
    return (
        <div className="newUser">
            <h1 className="newUserTitle">{title}</h1>
            <div className="newUserFlex">
                <form className="newUserForm" onSubmit={handleAddUser}>
                    <div className="newUserUpload">
                        <div className="newUserUpload">
                            <img className="newUserUploadImg" src={
                                file
                                    ? URL.createObjectURL(file)
                                    : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                            }
                                alt="" />
                            <label htmlFor="file"><DriveFolderUploadOutlined className="newUserUploadIcon" /></label>
                            <input type="file" id="file" onChange={(e) => setFile(e.target.files[0])} style={{ display: "none" }} />
                        </div>
                    </div>
                    <div className="newUserRight">
                        {inputs.map((input) => (
                            <div className="newUserFormInput" key={input.id}>
                                <label className="newUserTitleInput">{input.label}</label>
                                <input className="newUserInput" id={input.id} type={input.type} placeholder={input.placeholder} onChange={handleInput} />
                            </div>


                        ))}
                    </div>
                    <button disabled={per !== null && per < 100} type="submit" className="newUserBtn">Create</button>
                    <Link to="/users">
                        <button className="newUserBtn-back">Cancel</button>
                    </Link>
                </form>
            </div>
        </div>
    )
}
export default NewUser;