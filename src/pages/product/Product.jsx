import { Publish } from "@material-ui/icons"
import { Link } from "react-router-dom"
import Chart from "../../components/chart/Chart"
import { productData } from "../../dummyData"
import "./product.css"

export default function Product() {
    return (
        <div className="product">
            <div className="productTitleContainer">
                <h1 className="productTitle">Product</h1>
                <Link to="/newproduct">
                    <button className="productAddButton">Create</button>
                </Link>
            </div>
            <div className="productTop">
                <div className="productTopLeft">
                    <Chart data={productData} dataKey="Sales" title="Sales Performance" />
                </div>
                <div className="productTopRight">
                    <div className="productInfoTop">
                        <img className="productInfoImg" src="https://stcv4.hnammobile.com/downloads/3/danh-gia-iphone-14-pro-14-pro-max-lieu-co-du-suc-khuynh-dao-gioi-cong-nghe-11663319473.jpg" alt="" />
                        <span className="productName">Iphone Apple 14</span>
                    </div>
                    <div className="productInfoBottom">
                        <div className="productInfoItem">
                            <span className="productInfoKey">id:</span>
                            <span className="productInfoValue">123</span>
                        </div>
                        <div className="productInfoItem">
                            <span className="productInfoKey">sales:</span>
                            <span className="productInfoValue">3200</span>
                        </div>
                        <div className="productInfoItem">
                            <span className="productInfoKey">active:</span>
                            <span className="productInfoValue">yes</span>
                        </div>
                        <div className="productInfoItem">
                            <span className="productInfoKey">in stock:</span>
                            <span className="productInfoValue">no</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="productBottom">
                <form className="productForm">
                    <div className="productFormLeft">
                        <label ></label>
                        <input type="text" placeholder="Name Product" />
                        <label>In Stock</label>
                        <select name="inStock" id="idStock">
                            <option value="yes">Yes</option>
                            <option value="yes">No</option>
                        </select>
                        <label>In Stock</label>
                        <select name="active" id="active">
                            <option value="yes">Yes</option>
                            <option value="yes">No</option>
                        </select>
                    </div>
                    <div className="productFormRight">
                        <div className="productUpload">
                            <img className="productUploadImg" src="https://stcv4.hnammobile.com/downloads/3/danh-gia-iphone-14-pro-14-pro-max-lieu-co-du-suc-khuynh-dao-gioi-cong-nghe-11663319473.jpg" alt="" />
                            <label htmlFor="file">
                                <Publish />
                            </label>
                            <input type="file" id="file" style={{ display: "none" }} />
                        </div>
                        <button className="productBtn">Update</button>
                    </div>
                </form>
            </div>
        </div>
    )
}
