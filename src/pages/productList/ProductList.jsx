import { DeleteOutline, Edit } from "@mui/icons-material";
import { DataGrid } from '@mui/x-data-grid';
import { useState } from "react";
import { Link } from "react-router-dom";
import { productRows } from "../../dummyData";
import "./productList.css";

export default function ProductList() {
    const [product, setProduct] = useState(productRows);
    const handleDelete = (id) => {
        setProduct(product.filter((item) => item.id !== id));
    };

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        {
            field: 'img', headerName: 'Image', renderCell: (params) => {
                return (<div>
                    <img className="img_product" src={params.row.img} />
                </div>)
            }
        },
        { field: 'name', headerName: 'Name', width: 160 },
        { field: "stock", headerName: "Stock", width: 230 },
        {
            field: 'status',
            headerName: 'Status',
            width: 90,
        },
        {
            field: 'price',
            headerName: 'Price',
            width: 90,
        },
        {
            field: "action",
            headerName: 'Action',
            with: 140,
            renderCell: (params) => {
                return (
                    <>
                        <Link to={"/product/" + params.row.id}>
                            <button className="btn_action"><Edit className="productListEdit" /></button>
                        </Link>
                        <button className="btn_action"><DeleteOutline className="productListDelete" onClick={() =>
                            handleDelete(params.row.id)} /></button>
                    </>
                )
            }
        }
    ];

    return (
        <div className="productList">
            <DataGrid
                rows={product}
                disableRowSelectionOnClick
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: { page: 0, pageSize: 9 },
                    },
                }}
                pageSizeOptions={[5, 10]}
                checkboxSelection
            />
        </div>
    )
}
