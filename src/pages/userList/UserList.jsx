import { Link } from "react-router-dom";
import DataTableUser from "../../components/datatable/DataTableUser";
import "./userList.css";

const UserList = () => {

    return (
        <div className="userList">
            <Link to="/" className="link">
                <button className="userBtn-exit">Back</button>
            </Link>
            <h1 className="userListTitle">List User</h1>
            <Link to="/newUser">
                <button className="userAddButton">Create</button>
            </Link>
            <DataTableUser />
        </div>
    )
}
export default UserList;